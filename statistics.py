
import warnings
import numpy as np
import pandas as pd
import scipy.stats as st
import statsmodels as sm
import matplotlib

def auto_fit_bell_distr(data, bins=200, ax=None):
    """Finds the best fit for every bell-shaped distribution, then returns the best out of them, and the parameters for it
    
    Arguments:
    -----------
    data : array-like dataset
        1D array-like data (i.e. pd.Series)
    bins : int, optional (default : 200)\n
        number of bins to use when evaluating the distributions
    ax : matplotlib.axes, optional (default : None)\n
        if passed, will be used to plot the distributions 
    
    Returns:
    -----------
    out : str
        name of the best distribution
    out : tuple
        parameters for the best fit
    """
    if not isinstance(bins, int):
        raise TypeError('The argument bins must be an integer')
    if not isinstance(ax, matplotlib.pyplot.Axes) and ax is not None:
        raise TypeError('The argument ax must be a matplotlib axes, or None')
    if not hasattr(data, "__len__") or isinstance(data, str):
        raise TypeError('The argument data is not array-like')
    # Get histogram of original data
    y, x = np.histogram(data, bins=bins, density=True)
    x = (x + np.roll(x, -1))[:-1] / 2.0

    # Distributions to check
    DISTRIBUTIONS = [        
        st.alpha,st.anglit,st.beta,st.betaprime,st.burr,st.cauchy,st.chi,st.chi2,st.cosine,
        st.dgamma,st.dweibull,st.erlang,st.exponnorm,st.exponweib,st.fatiguelife,st.fisk,
        st.foldcauchy,st.foldnorm,st.frechet_r,st.frechet_l,st.genlogistic,st.gennorm,
        st.gausshyper,st.gamma,st.gengamma,st.gumbel_r,
        st.gumbel_l,st.hypsecant,st.invgamma,st.invgauss,
        st.invweibull,st.johnsonsb,st.laplace,st.levy,st.levy_l,st.levy_stable,
        st.logistic,st.loggamma,st.loglaplace,st.lognorm,st.maxwell,st.mielke,st.nakagami,
        st.nct,st.norm,st.pearson3,st.powerlognorm,st.powernorm,st.rdist,
        st.rayleigh,st.rice,st.recipinvgauss,st.semicircular,st.t,st.triang,st.truncnorm,
        st.vonmises,st.vonmises_line,st.wald,st.weibull_min,st.weibull_max
    ]

    # Best holders
    best_distribution = st.norm
    best_params = (0.0, 1.0)
    best_sse = np.inf

    # Estimate distribution parameters from data
    for distribution in DISTRIBUTIONS:

        # Try to fit the distribution
        try:
            # Ignore warnings from data that can't be fit
            with warnings.catch_warnings():
                warnings.filterwarnings('ignore')

                # fit dist to data
                params = distribution.fit(data)

                # Separate parts of parameters
                arg = params[:-2]
                loc = params[-2]
                scale = params[-1]

                # Calculate fitted PDF and error with fit in distribution
                pdf = distribution.pdf(x, loc=loc, scale=scale, *arg)
                sse = np.sum(np.power(y - pdf, 2.0)) #pylint: disable=E1101

                # if axis pass in add to plot
                try:
                    if ax:
                        vals = pd.Series(pdf, x)
                        vals.name = distribution.name
                        vals.plot(ax=ax)
                except Exception:
                    pass

                # identify if this distribution is better
                if best_sse > sse > 0:
                    best_distribution = distribution
                    best_params = params
                    best_sse = sse

        except Exception:
            pass

    return (best_distribution.name, best_params)

def make_pdf(dist, params, size=10000):
    """retrieve the scipy.stats distribution based on the name, and build a descrete representation of it
    
    Arguments:
    -----------
    dist : str
        distribution name (from scipy.stats)
    params : list or tuple
        arguments (from scipy.stats)
    size : int 
        size of the array to return
    
    Returns:
    -----------
    out : pd.Series
        descrete (=size) representation of the continuous distribution 
    """
    if isinstance(dist, str):
        dist = getattr(st, dist)
    if not isinstance(params, tuple) and not isinstance(params, list):
        raise TypeError('The argument params is not a tuple or list')
    if not isinstance(size, int):
        raise TypeError('The argument size must be an integer')
    
    # Separate parts of parameters
    arg = params[:-2]
    loc = params[-2]
    scale = params[-1]

    # Get sane start and end points of distribution
    start = dist.ppf(0.01, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.01, loc=loc, scale=scale)
    end = dist.ppf(0.99, *arg, loc=loc, scale=scale) if arg else dist.ppf(0.99, loc=loc, scale=scale)

    # Build PDF and turn into pandas Series
    x = np.linspace(start, end, size)
    y = dist.pdf(x, loc=loc, scale=scale, *arg)
    pdf = pd.Series(y, x)

    return pdf

def random_from_pdf(pdf, size):
    """Generates random numbers from a probability distribution function
    
    Arguments:
    -----------
    pdf : pd.Series
        probability distribution, having index=X and value=frequency
    size : int 
        number of random numbers needed

    Returns:
    -----------
    out : pd.Series
        random numbers generated
    """
    if not isinstance(pdf, pd.Series):
        raise TypeError('The argument pdf is not a Series')
    if not isinstance(size, int):
        raise TypeError('The argument size must be an integer')
    random_n = []
    pdf /= pdf.sum()
    x = pdf.index.tolist()
    y = pdf.values
    for i in range(size):
        val = np.random.choice(x, p=y) #pylint: disable=E1101
        random_n.append(val)

    return pd.Series(random_n)


