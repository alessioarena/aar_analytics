import os
import sys
import warnings
import pandas as pd

from aar_analytics.utils import available_cpu_count, coerce_to_series, make_path
from sklearn.exceptions import NotFittedError

#####################################
# Subfunctions ######################

def fit(X_fit, y_fit, regressor):
    """Performs the model fit

    Arguments:
    -----------
    X_fit : pd.DataFrame
        features to fit the model against
    y_fit : pd.Series 
        variable to predict
    regressor : sklearn regressor)
        sklearn or similar regressor object

    Returns:
    -----------
    out : sklearn regressor
        fitted regressor
    """
    if not isinstance(X_fit, pd.DataFrame) and not isinstance(X_fit, pd.Series):
        raise TypeError('The argument X_fit must be a Dataframe or Series')
    if not isinstance(y_fit, pd.Series):
        try:
            y_fit = coerce_to_series(y_fit)
        except RuntimeError:
            raise TypeError('The argument y_fit must be a Series')
    if not sorted(X_fit.index.tolist()) == sorted(y_fit.index.tolist()):
        raise ValueError('X_fit and y_fit must have the exact same index')

    if any(y_fit.isna()):
        warnings.warn('Detected NaN values in y_fit. Those will be removed')
        y_fit = y_fit.dropna()
        X_fit = X_fit.loc[y_fit.index, :]
    X_fit = X_fit.values
    try:
        model = regressor.fit(X_fit, y_fit)
    except AttributeError:
        raise TypeError('The argument regressor is not a valid sklearn object')
    return model

def predict(X_pred, model):
    """Performs the prediction using an already trained model

    Arguments:
    -----------
    X_pred : pd.DataFrame
        features to predict against
    model : sklear-style model 
        trained model to use

    Returns:
    -----------
    out : pd.Series
        y_pred
    """
    if not isinstance(X_pred, pd.DataFrame) and not isinstance(X_pred, pd.Series):
        raise TypeError('The argument X_pred must be a Dataframe or Series')
    try:
        y_pred = model.predict(X_pred)
    except NotFittedError:
        raise NotFittedError('The model is not fitted. Please use the fit function before trying to predict')
    except AttributeError:
        raise TypeError('The argument model is not a valid sklearn regressor')
    y_pred = pd.Series(y_pred, index=X_pred.index)

    return y_pred

def test(X_pred, y_pred_actual, model):
    """Performs the testing against a ground truth data (actual)
    
    Arguments:
    -----------
    X_pred : pd.DataFrame
        Features to predict against
    y_pred_actual : pd.Series 
        ground truth for the prediction
    model : sklear-style model 
        trained model to use
    
    Returns:
    -----------
    out : float
        R squared score
    """
    if not isinstance(X_pred, pd.DataFrame) and not isinstance(X_pred, pd.Series):
        raise TypeError('The argument X_pred must be a Dataframe or Series')
    if not isinstance(y_pred_actual, pd.Series):
        try:
            y_pred_actual = coerce_to_series(y_pred_actual)
        except RuntimeError:
            raise TypeError('The argument y_pred_actual must be a Series')
    
    y_pred_actual = y_pred_actual.dropna()
    ground_truth_score = model.score(X_pred.loc[y_pred_actual.index, :], y_pred_actual)
    return ground_truth_score

def print_score(fit_score=False, predict_score=False, oob_score=False, features_importance=False, stream=sys.stdout):
    print("                              ", file=stream)
    print("______________________________", file=stream)
    print("______modelling results_______", file=stream)
    print("                              ", file=stream)
    if fit_score is not False:
        print(" fit score :             %1.4f" % fit_score, file=stream)
    if oob_score is not False:
        print(" oob score :             %1.4f" % oob_score, file=stream)
    if predict_score is not False:
        print(" predict score :         %1.4f" % predict_score, file=stream)
    if features_importance is not False:
        print("                              ", file=stream)
        print(" features importance :        ", file=stream)
        print("%s" % features_importance, file=stream)
    return None

#####################################
# Callers      ######################

def ET_regression(X_fit=False, y_fit=False, X_pred=False, y_pred_actual=False, model=False, random_state=0, n_estimators=500, verbose=True, log=False):
    """ExtraTree regression
    This function can:
    - Fit the model
    - Perform the prediction using a built or loaded model
    - Test the prediction against the ground truth

    Arguments:
    -----------
    X_fit : pd.DataFrame, optional (Default : False)
        features to train against
    y_fit : pd.Series, optional (Default : False)
        ground truth for the training
    X_pred : pd.DataFrame, optional (Default : False)
        features to predict against
    y_pred_actual : pd.DataFrame, optional (Default : False)
        ground truth for y_pred (for testing purpose)
    model : sklearn model, optional (Default : False)
        already trained model
    random_state : int, optional (Default : 0)
        random state for the Extra Tree regressor initialization
    verbose : bool, optional (Default : True)
        Prints the R^2 score and features importance

    Returns:
    -----------
    out : sklearn model, if X_fit/y_fit are passed
        Trained model
    out : pd.Series, if X_pred is passed
        predicted y_pred using X_pred and model built
    out : pd.DataFrame, if quantiles argument is passed
        quantile predictions
    """

    bool_fit = False
    bool_predict = False
    bool_test = False
    if (X_fit is not False) and (y_fit is not False):
        if (model is not False):
            raise RuntimeError('You cannot pass X_fit/y_fit and model arguments together')
        bool_fit = True
    if (X_pred is not False):
        if not model and not bool_fit:
            raise RuntimeError('Values cannot be predicted unless a model is passed or built')
        bool_predict = True
    if (y_pred_actual is not False):
        if not bool_predict:
            raise RuntimeError('Cannot test the model if a prediction is not performed')
        bool_test = True

    if bool_fit and bool_predict:
        if not sorted(X_fit.columns.tolist()) == sorted(X_pred.columns.tolist()):
            raise ValueError('X_fit and X_pred must have the exact same columns')
    if bool_fit and bool_test:
        if not sorted(X_pred.index.tolist()) == sorted(y_pred_actual.index.tolist()):
            raise ValueError('X_pred and y_pred_actual must have the exact same index')

    if not isinstance(random_state, int):
        raise TypeError('The argument randome_state must be an integer')
    if not isinstance(n_estimators, int):
        raise TypeError('The argument n_estimators must be int')
    if not isinstance(verbose, bool):
        raise TypeError('The argument verbose must be a boolean')
    if not isinstance(log, str) and log:
        raise TypeError('The argument log must be a filename (string) or False')


    if bool_fit:
        from sklearn.ensemble import ExtraTreesRegressor
        reg = ExtraTreesRegressor(random_state=random_state, n_estimators=n_estimators, oob_score=True, n_jobs=available_cpu_count(), bootstrap=True)
        feat_idx = X_fit.columns
        model = fit(X_fit, y_fit, reg)
        fit_score = model.score(X_fit, y_fit)
        oob_score = model.oob_score_
        features = pd.DataFrame(model.feature_importances_, index=feat_idx, columns=['weight'])
    else:
        oob_score = False
        fit_score = False
        features = False

    if bool_predict:
        oob_score = model.oob_score_
        y_pred = predict(X_pred, model)

    if bool_test:
        predict_score = test(X_pred, y_pred_actual, model)
    else:
        predict_score = False

    if not any([bool_fit, bool_predict]):
        raise RuntimeError('No operation could be performed. Make sure to pass at least one of the following argument combinations: (X_fit, y_fit), (model, X_pred), (X_fit, y_fit, X_pred)')
    if verbose:
        print_score(fit_score, predict_score, oob_score, features)
    if log:
        with open("test.txt", "a") as myfile:
            print_score(fit_score, predict_score, oob_score, features, stream=myfile)

    if bool_fit:
        if bool_predict:
            return model, y_pred
        return model
    if bool_predict:
        return y_pred

def quantile_ET_regression(X_fit=False, y_fit=False, X_pred=False, y_pred_actual=False, model=False, quantiles=False, random_state=0, n_estimators=500, verbose=True, log=False):
    """ExtraTreeQuantile regression
    This function can:
    - Fit the model
    - Perform the prediction using a built or loaded model
    - Test the prediction against the ground truth
    - Predict the quantiles using a built or loaded model

    Arguments:
    -----------
    X_fit : pd.DataFrame, optional (Default : False)
        features to train against
    y_fit : pd.Series, optional (Default : False)
        ground truth for the training
    X_pred : pd.DataFrame, optional (Default : False)
        features to predict against
    y_pred_actual : pd.DataFrame, optional (Default : False)
        ground truth for y_pred (for testing purpose)
    model : skgarden model, optional (Default : False)
        already trained model
    quantiles : list of int, optional (Default : False, example [90, 10])
        list of quantiles to be calculated. False to skip it
    random_state : int, optional (Default : 0)
        random state for the Extra Tree regressor initialization
    verbose : bool, optional (Default : True)
        Prints the R^2 score and features importance

    Returns:
    -----------
    out : skgarden model, if X_fit/y_fit are passed
        Trained model
    out : pd.Series, if X_pred is passed
        predicted y_pred using X_pred and model built
    out : pd.DataFrame, if quantiles argument is passed
        quantile predictions
    """

    bool_fit = False
    bool_predict = False
    bool_test = False
    if (X_fit is not False) and (y_fit is not False):
        if (model is not False):
            raise RuntimeError('You cannot pass X_fit/y_fit and model arguments together')
        bool_fit = True
    if (X_pred is not False):
        if not model and not bool_fit:
            raise RuntimeError('Values cannot be predicted unless a model is passed or built')
        bool_predict = True
    if (y_pred_actual is not False):
        if not bool_predict:
            raise RuntimeError('Cannot test the model if a prediction is not performed')
        bool_test = True
    if quantiles:
        if not model and not bool_fit:
            raise RuntimeError('Quantiles values cannot be predicted unless a model is passed or built')

    if bool_fit and bool_predict:
        if not sorted(X_fit.columns.tolist()) == sorted(X_pred.columns.tolist()):
            raise ValueError('X_fit and X_pred must have the exact same columns')
    if bool_fit and bool_test:
        if not sorted(X_pred.index.tolist()) == sorted(y_pred_actual.index.tolist()):
            raise ValueError('X_pred and y_pred_actual must have the exact same index')

    if (not isinstance(quantiles, list) or not all([isinstance(x, int) for x in quantiles])) and quantiles:
        raise TypeError('The argument quantiles must be a list of integers, or False')
    if not isinstance(random_state, int):
        raise TypeError('The argument randome_state must be an integer')
    if not isinstance(n_estimators, int):
        raise TypeError('The argument n_estimators must be int')
    if not isinstance(verbose, bool):
        raise TypeError('The argument verbose must be a boolean')
    if not isinstance(log, str) and log:
        raise TypeError('The argument log must be a filename (string) or False')


    if quantiles and not all([(x <= 100) and (x >= 0) for x in quantiles]):
        raise ValueError('At least one specified quantile is not between 0 and 100')


    if bool_fit:
        from skgarden import ExtraTreesQuantileRegressor
        reg = ExtraTreesQuantileRegressor(random_state=random_state, n_estimators=n_estimators, oob_score=True, n_jobs=available_cpu_count(), bootstrap=True)
        feat_idx = X_fit.columns
        model = fit(X_fit, y_fit, reg)
        fit_score = model.score(X_fit, y_fit)
        oob_score = model.oob_score_
        features = pd.DataFrame(model.feature_importances_, index=feat_idx, columns=['weight'])
    else:
        oob_score = False
        fit_score = False
        features = False

    if bool_predict:
        y_pred = predict(X_pred, model)

    if bool_test:
        predict_score = test(X_pred, y_pred_actual, model)
    else:
        predict_score = False

    if quantiles:
        q_data = []
        q_cols = []
        for q in quantiles:
            try:
                q_pred = model.predict(X_pred, quantile=q)
            except TypeError:
                raise RuntimeError('The model argument is not a valid skgarden model (quantile)')
            q_data.append(q_pred)
            q_cols.append(str(q)+'_quantile')
        q_data = pd.concat(q_data, axis=1)
        q_data.columns = q_cols

    if not any([bool_fit, bool_predict, bool(quantiles)]):
        raise RuntimeError('No operation could be performed. Make sure to pass at least one of the following argument combinations: (X_fit, y_fit), (model, X_pred), (X_fit, y_fit, X_pred)')
    if verbose:
        print_score(fit_score, predict_score, oob_score, features)
    if log:
        if os.path.isfile(log):
            os.remove(log)
        with open(log, "a") as myfile:
            print_score(fit_score, predict_score, oob_score, features, stream=myfile)


    if bool_fit:
        if bool_predict:
            if quantiles:
                return model, y_pred, q_data
            return model, y_pred
        return model
    if bool_predict:
        if quantiles:
            return y_pred, q_data
        return y_pred
    if quantiles:
        return q_data

#####################################
# Utilities    ######################

def calculate_residuals(y_pred, y_actual):
    if not isinstance(y_pred, pd.Series):
        try:
            y_pred = coerce_to_series(y_pred)
        except RuntimeError:
            raise TypeError('The argument y_pred must be a Series')
    if not isinstance(y_actual, pd.Series):
        try:
            y_pred = coerce_to_series(y_pred)
        except RuntimeError:
            raise TypeError('The argument y_actual must be a Series')
    if not sorted(y_pred.index.tolist()) == sorted(y_actual.index.tolist()):
        raise ValueError('y_pred and y_actual must have the exact same index')

    residuals = y_pred - y_actual
    return residuals

def save_model(model, path):
    """Small utility to save models
    
    Arguments:
    -----------
    model : sklearn-style model
        trained model (It will check if it is trained and complain otherwise)
    path : str 
        path including filename
    """
    if not isinstance(path, str):
        raise TypeError('The argument path is not a string')

    try:
        features = model.n_features_
    except AttributeError:
        raise TypeError('The model argument is not a valid fitted regressor')

    from numpy import zeros
    test_data = zeros((3, features))
    try:
        model.predict(test_data)
    except NotFittedError:
        raise NotFittedError('The model is still not fitted. Do you really want to save an empty model?')
    
    if path.endswith('/'):
        raise ValueError('The specified path does not contain a filename')
    if len(path.split('/')) > 1:
        fld_path = make_path(path.split('/')[:-1])
        if not os.path.isdir(fld_path):
            os.makedirs(fld_path)

    import joblib
    joblib.dump(model, path, compress=3)
    return None

def load_model(path):
    """Utility to load models

    Arguments:
    -----------
    path : str
        path including filename

    Returns:
    -----------
    out : sklearn trained model
        The previously saved model
    """
    if not isinstance(path, str):
        raise TypeError('The argument path is not a string')
    if not os.path.isfile(path):
        raise FileNotFoundError('The specified model does not exist')
    import joblib
    return joblib.load(path)
