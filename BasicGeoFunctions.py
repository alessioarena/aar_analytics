"""Set of basic functions to manipulate georeferenced data"""
import warnings
import pandas as pd
import geopandas as gpd
import shapely as shpy
import numpy as np
import enea_analytics.ParaPandas as PP
from sklearn.neighbors import NearestNeighbors
from multiprocessing import Pool
import re

def recover_coordinates(i_data):
    """Small utility to recover the coordinates information when GeoDataFrame are accidentally saved as csv.

    This in fact will cause the geometry column to be converted to plain text, and therefore not be readily available anymore.
    Implemented just for points

    Arguments:
    -----------
    i_data : pd.DataFrame
        must contain a geometry column

    Returns:
    -----------
    out : pd.DataFrame
        inclusive of Lat and Lon columns
    """


    if isinstance(i_data, gpd.GeoDataFrame):
        raise TypeError('The input file is already a GeoDataFrame')
    if not isinstance(i_data, pd.DataFrame):
        raise TypeError('The input data must be a DataFrame')
    if not set(['geometry']).issubset(i_data.columns):
        raise ValueError('The data does not contain the geometry column')

    if not isinstance(i_data.loc[:, 'geometry'].iloc[0], str):
        i_data.loc[:, 'geometry'] = i_data.geometry.astype(str)
    i_data.loc[:, 'Lat'] = i_data.geometry.apply(lambda x: re.findall('-?[0-9.]+', x)[1] if len(re.findall('-?[0-9.]+', x)) > 0 else np.nan)
    i_data.loc[:, 'Lon'] = i_data.geometry.apply(lambda x: re.findall('-?[0-9.]+', x)[0] if len(re.findall('-?[0-9.]+', x)) > 0 else np.nan)
    return i_data


# This function converts a pd.DataFrame to gpd.GeoDataFrame using lat and long fields
def data_to_geodata(i_data, lat='lat', lon='long', lat2=False, lon2=False, epsg=4283, drop=True):
    """Converts a pd.DataFrame to gpd.GeoDataFrame based on lat and long columns (and lat2, long2 in case of lines)

    Arguments:
    -----------
    i_data : pd.DataFrame
        input dataframe to convert to geodataframe
    lat : str
        column name to source the latitude for the first point of the geometry
    lon : str
        column name to source the longitude for the first point of the geometry
    lat2 : str or False, optional (default: False)\n
        column name to source the latitude for the second point of the geometry
    lon2 : str or False, optional (default: False)\n
        column name to source the longitude for the second point of the geometry
    epsg : int, optional (default: 4283)\n
        epsg code (coordinate reference system) to assign to the new spatial data
    drop : bool, optional (default: True)\n
        drop the lat, lon (and lat2, lon2) columns

    Returns:
    -----------
    out : gpd.GeoDataFrame
        converted geodataframe
    """

    if not isinstance(i_data, pd.DataFrame):
        raise TypeError('The input data is not a pd.DataFrame')
    if isinstance(i_data, gpd.GeoDataFrame):
        raise TypeError('The input data is already a gpd.GeoDataFrame')
    if i_data.empty:
        raise ValueError('The input data is empty')
    if not set([lat, lon]).issubset(i_data.columns):
        raise KeyError('Missing columns '+str(lat)+' and '+str(lon)+' in the input file')

    if lat2 and lon2:
        if set([lat2, lon2]).issubset(i_data.columns):
            o_data = i_data.dropna(subset=[lat, lon, lat2, lon2], how='any')
            if len(o_data) != len(i_data):
                warnings.warn('There are '+str(len(i_data)-len(o_data))+' invalid geometries. Dropping those features', RuntimeWarning, stacklevel=2)
            geometry = [shpy.geometry.LineString([(x,y),(x2,y2)]) for x,y,x2,y2 in zip(o_data[str(lon)].astype(float).values, o_data[str(lat)].astype(float).values, o_data[str(lon2)].astype(float).values, o_data[str(lat2)].astype(float).values)]
            if drop: 
                o_data = o_data.drop([lon, lat, lon2, lat2], axis=1)
            crs = {'init': 'epsg:'+str(epsg)}
            o_geodata = gpd.GeoDataFrame(o_data, crs=crs, geometry=geometry)
        else:
            raise KeyError('Missing columns '+str(lat2)+' and '+str(lon2)+' in the input file')
    else:
        o_data = i_data.dropna(subset=[lat, lon], how='any')
        if len(o_data) != len(i_data):
            warnings.warn('There are '+str(len(o_data)-len(o_data))+' invalid geometries. Dropping those features', RuntimeWarning, stacklevel=2)
        geometry = [shpy.geometry.Point(xy) for xy in zip(o_data[str(lon)].astype(float).values, o_data[str(lat)].astype(float).values)]
        if drop: 
            o_data = o_data.drop([lon, lat], axis=1)
        crs = {'init': 'epsg:'+str(epsg)}
        o_geodata = gpd.GeoDataFrame(o_data, crs=crs, geometry=geometry)

    return o_geodata

# Routine to extract lat long information out of a Point or LineString gpd.GeoDataFrame
# can be used with PP.parapandas
def extract_point_coords(gdb, epsg_reproj=False):
    """Extracts coordinates information from a GeoDataFrame of shapely.geometry.Points or shapely.geometry.LineString
    
    Arguments:
    -----------
    gdb : gpd.GeoDataFrame
        Input GeoDataFrame
    epsg_reproj : int or bool, optional (default : Value)\n
        epsg reprojection code, or False to leave it as it is
    
    Returns:
    -----------
    out : np.ndarray
        2D array of shape col:2, rows:len(gdb) or len(gdb)*2 for LineString. This contains the coordinates
    out : np.ndarray
        1D array of shape rows:len(gdb) or len(gdb)*2 for LineString. This contains the index fof gdb
    """
    if not isinstance(gdb, gpd.GeoDataFrame):
        raise TypeError('The input is not a GeoDataFrame')
    if gdb.empty:
        raise ValueError('The input data is empty')

    if isinstance(gdb.iloc[0].geometry, shpy.geometry.Point):
        ##  Optional reprojection of data
        if epsg_reproj: gdb = gdb.to_crs(epsg=epsg_reproj)
        coord = np.zeros((len(gdb), 2))

        mask = gdb.geometry.isnull()
        temp = gdb.geometry.apply(lambda x: x.coords[0][:2] if x.coords[0] == x.coords[0] else 0)
        coord[:, 0] = temp.apply(lambda x: x[0]).tolist()
        coord[:, 1] = temp.apply(lambda x: x[1]).tolist()
        coord_idx = gdb.index.to_series()
        coord_idx.loc[mask] = np.nan
        coord_idx = coord_idx.as_matrix()

        return coord, coord_idx
    elif isinstance(gdb.iloc[0].geometry, shpy.geometry.LineString): #FIXME updating the line one
        ##  Optional reprojection of data
        if epsg_reproj: gdb = gdb.to_crs(epsg=epsg_reproj)
        coord = np.zeros((1, 2))
        coord_idx = np.zeros((1, 1))
            ##  Iterate through each element to extract the geometry. Not possible in bulk
        for enu, idx in enumerate(gdb.index):
            geom = gdb.loc[idx].geometry.coords
            for val in geom:
            ##  Check for nan geometries
                if val == val:
                    coord = np.append(coord, np.array(val[:2]).reshape(1, 2), axis=0)
                    coord_idx = np.append(coord_idx, idx)
        return np.delete(coord, [0], axis=0), np.delete(coord_idx, [0], axis=0)
    else: 
        raise NotImplementedError('The input GeoDataFrame does not cointain shapely.geometry.Point or LineString')


# Nearest Neighbor search for Point geometries, plus merging
# canNOT be used with PP.parapandas
# uses internally PP.parapandas and extract_point_coords
def merge_by_location(i_gdb1, i_gdb2, max_distance=30, suffixes=False, output_distance=False, verbose=True):
    """Merges the features of the two geodataframes based on a nearest neighbor approach
    Empty geometries will be dropped in the process.

    Arguments:
    -----------
    i_gdb1 : gpd.GeoDataFrame
        first geodataframe, smaller
    i_gdb2 : gpd.GeoDataFrame 
        second geodataframe, larger
    max_distance : int, optional (default : 30)
        maximum distance in meters between two points
    suffixes : tuple of [str] or False, optional (default : False)\n
        suffixes to use for the merge operation (if same column names are encountered)
    output_distance : bool, optional (default : False)\n
        output_distanceopment run
    verbose : bool, optional (default : True)\n
        print progress

    Returns:
    -----------
    out : gpd.GeoDataFrame
        merged geodataframes on i_gdb1
    4xout : gpd.GeoDataFrame
        o_matched_left, o_matched_right, o_unmatched_left, o_unmatched_right if output_distance == True
    """

    if not isinstance(max_distance, int):
        raise TypeError('Only integer values are accepted for max_distance')
    if not isinstance(output_distance, bool):
        raise TypeError('output_distance argument must be a boolean value')
    if suffixes != False and not (isinstance(suffixes, tuple) and len(suffixes) == 2 and all(isinstance(x, str) for x in suffixes)):
        raise TypeError('Suffixes argument must be a tuple of two strings, or False') 

    ## Running the nearest neighbor search
    i_gdb1, distance = nearest_neighbor_search(i_gdb1, i_gdb2, verbose=verbose, output_distance=True)

    ## removing points with a distance greater than max_distance
    distance = distance.reshape(len(distance))
    mask_distance = np.greater_equal(distance, max_distance)
    i_gdb1.loc[mask_distance, 'IDX_MATCH'] = np.nan

    if not suffixes:
        suffixes = ('_x','_y')
    if not output_distance:
        o_gdb_merged = i_gdb1.merge(i_gdb2.drop(labels=['geometry'], axis=1), right_index=True, left_on='IDX_MATCH', how='left', suffixes=suffixes)
        o_gdb_merged.drop('IDX_MATCH', axis=1, inplace=True)
        return o_gdb_merged
    else:
        o_matched_right = i_gdb2[i_gdb2.index.isin(i_gdb1['IDX_MATCH'])]
        o_matched_left = i_gdb1[i_gdb1['IDX_MATCH'].notnull()]
        o_unmatched_right = i_gdb2[~i_gdb2.index.isin(i_gdb1['IDX_MATCH'])]
        o_unmatched_left = i_gdb1[i_gdb1['IDX_MATCH'].isnull()]
        return o_matched_left, o_matched_right, o_unmatched_left, o_unmatched_right

# quick generation of a rectangular polygon representing the bounding box of a geometry
def bounding_box(i_gdb):
    """Function to calculate the bounding box of a geodataframe
    
    Arguments:
    -----------
    i_gdb : gpd.GeoDataFrame
        input geodataframe
    
    Returns:
    -----------
    out : shapely.geometry.Polygon
        the bounding box
    """
    if not isinstance(i_gdb, gpd.GeoDataFrame):
        raise TypeError('The input data is not a GeoDataFrame')
    if i_gdb.empty:
        raise ValueError('The input GeoDataFrame is empty')

    bounds = i_gdb.geometry.bounds
    xmax = bounds.maxx.max()
    xmin = bounds.minx.min()
    ymax = bounds.maxy.max()
    ymin = bounds.miny.min()

    o_bb = shpy.geometry.Polygon([(xmax, ymax), (xmin, ymax), (xmin, ymin), (xmax, ymin)])
    return o_bb


def convex_hull(i_gdb):
    """Description
    
    Arguments:
    -----------
    i_gdb : gpd.GeoDataFrame
        description
    
    Returns:
    -----------
    out : shapely.geometry.Polygon
        the convex hull for the dataset
    """
    if not isinstance(i_gdb, gpd.GeoDataFrame) and not isinstance(i_gdb, gpd.GeoSeries):
        raise TypeError('The input must be a GeoDataFrame or GeoSeries')
    if len(i_gdb) < 3:
        raise IOError('The DataFrame contains less than 3 geometries')
    if isinstance(i_gdb, gpd.GeoDataFrame):
        if not all(isinstance(x, shpy.geometry.Point) for x in i_gdb.geometry):
            raise TypeError('At least one of the geomtries is not a Point')
        points = shpy.geometry.MultiPoint(i_gdb['geometry'].tolist())
    else:
        if not all(isinstance(x, shpy.geometry.Point) for x in i_gdb):
            raise TypeError('At least one of the geomtries is not a Point')
        points = shpy.geometry.MultiPoint(i_gdb.tolist())

    geom = points.convex_hull
    
    if geom.length == 0 or geom.area == 0:
        raise RuntimeError('The resulting convex hull appears to be a point')
    else:
        return geom

# quick routine to convert lat long point coordinates to LineString geometries
# can be used with PP.parapandas
def points_to_lines(point_start, point_end, verbose=True):
    '''This function takes two GeoDataFrames of Point objects having same lenght and index, and converts them to one GeoDataFrame of LineString objects\n
    Input\n
    point_start:    gpd.GeoDataFrame - list of first points for each line
    point_end:      gpd.GeoDataFrame - list of second points for each line
    verbose:        bool - verbose option (optional, False)
    Output:\n
    o_lines:        gpd.GeoDataFrame - list of LineString objects'''
    if isinstance(point_start, gpd.GeoDataFrame) and isinstance(point_end, gpd.GeoDataFrame):
        if not (point_start.empty) and (not point_end.empty):
            if len(point_start) == len(point_end):
                if (point_start.index == point_end.index).all():
                    if verbose: print('There are '+str(len(point_start))+' point tuples to be processed')
                    o_lines = gpd.GeoDataFrame(index=point_start.index, columns=['geometry'])
                    for idx in point_start.index:
                        if verbose: print(str(idx)+'\r', end="")
                        try:
                            sta = point_start.loc[idx, 'geometry'].coords[0]
                            end = point_end.loc[idx, 'geometry'].coords[0]
                        except AttributeError:
                            warnings.warn('Index '+str(idx)+' has no valid geometry', RuntimeWarning, stacklevel=2)
                            continue
                        
                        line = shpy.geometry.LineString([sta, end])
                        o_lines.loc[idx, 'geometry'] = line
                    return o_lines
                else: raise ValueError('The two input GeoDataFrame must have identical index')
            else: raise ValueError('The two input GeoDataFrame must have the same length')
        else: raise ValueError('At least one of the two input GeoDataFrame are empty')
    else: raise TypeError('At least one of the two input dataset are not GeoDataFrame')

# routine to match LineString objects based on location and orientation
# canNOT be used with PP.parapandas, but it is fast
# it uses internally PP.parapandas (unless preprocessed=True) and _line_matching_data_prep
def line_matching(i_1, i_idx1, i_2, i_idx2, preprocessed=True, output_distance=False, extended_output=False):
    if (len(i_1) == len(i_idx1)) and (len(i_2) == len(i_idx2)):
        if preprocessed:
            db1 = i_1
        else:
            db1 = PP.parapandas(_line_matching_data_prep, args=[i_1, i_idx1])
        if preprocessed:
            db2 = i_2
        else:
            db2 = PP.parapandas(_line_matching_data_prep, args=[i_2, i_idx2])

        ## Shaping the data for the Nearest Neighbors
        mask = np.isnan(db1[:, -1])
        mask2 = np.isnan(db2[:, -1])
        db1_nn = db1[~mask]
        db2_nn = db2[~mask2]
        i_idx1_nn = i_idx1[~mask]
        i_idx2_nn = i_idx2[~mask2]
        
        nbgrs = NearestNeighbors(n_neighbors=1, radius=30, algorithm='ball_tree').fit(db2_nn)
        if output_distance:
            distance, match= nbgrs.kneighbors(db1_nn, return_distance=True)
        else:
            match = nbgrs.kneighbors(db1_nn, return_distance=False)
        if extended_output: 
            if output_distance: return match, i_idx1_nn, db1_nn, i_idx2_nn, distance
            else: return match, i_idx1_nn, db1_nn, i_idx2_nn
        elif output_distance:
            return match, i_idx1_nn, i_idx2_nn, distance
        else: return match, i_idx1_nn, i_idx2_nn
    else: raise ValueError('The length of the input dataset and its index dataset are not matching')

# routine to prepare the data for line_matching
# it calculates the middle point and orientation(rho) for each line segment
# please use extract_point_coords for the input
# can be used with PP.parapandas(not perfect though)
def _line_matching_data_prep(idx, points):
    np.seterr(all='raise')
    Theta = np.full(len(points), fill_value=np.nan)
    r = np.full(len(points), fill_value=np.nan)
    mid_p = np.full((len(points),2), fill_value=np.nan)
    ## converting the lines to polar coordinates (Hesse normal form)
    for i, point in enumerate(points[:-1]):
        if idx[i] == idx[i+1]:
            try:
                Theta[i] = np.arctan(-(point[0] - points[i+1, 0])/(point[1] - points[i+1, 1])) #pylint: disable=E1101
            except FloatingPointError:
                Theta[i] = np.arctan(np.inf) #pylint: disable=E1101
            #r[i] = point[0] * np.cos(Theta[i]) + point[1] * np.sin(Theta[i])
            ## Calculating the middle point of the line
            mid_p[i] = np.array([(point[0] + points[i+1, 0])/2, (point[1] + points[i+1, 1])/2 ])
    ## creating the database including theta, rho and middle point for nearest neighbor matching
    db = np.append(mid_p, Theta.reshape(len(Theta), 1), axis=1)    
    #db = np.append(db, r.reshape(len(r), 1), axis=1)
    return db

# Nearest Neighbor search for Point features, with no merging
# for LineString features please use line_matching
# it uses internally PP.parapandas
def nearest_neighbor_search(i_gdb1, i_gdb2, verbose=False, output_distance=False, n_neighbors=1):
    """Nearest neighbors search on GeoDataFrame of Points, using the sklearn algorithm

    Arguments:
    -----------
    i_gdb1 : gpd.GeoDataFrame
        Geodataframe containing features to match
    i_gdb2 : gpd.GeoDataFrame 
        Geodataframe containing reference features
    verbose : bool, optional (default : False)\n
        print some text on screen
    output_distance : bool, optional (default : False)\n
        output the distance in meters to the nearest neighbor
    n_neighbors : int, optional (default : 1)\n
        number of neighbors to search
    
    Returns:
    -----------
    out : gpd.GeoDataFrame
        First GeoDataFrame with an additional column reporting the index of the closest neighbor
    """
    
    if not isinstance(output_distance, bool):
        raise TypeError('output_distance argument must be a boolean value')
    if not isinstance(verbose, bool):
        raise TypeError('verbose argument must be a boolean value')
    if not isinstance(i_gdb1, gpd.GeoDataFrame) or not isinstance(i_gdb2, gpd.GeoDataFrame):
        raise TypeError('The two inputs must be geodataframes')
    if i_gdb1.empty or i_gdb2.empty:
        raise ValueError('One of the two inputs is an empty geodataframe')

    if verbose: print('Dropping empty geometries...', end='')
    i_gdb1 = i_gdb1.copy()
    i_gdb1 = i_gdb1.loc[~i_gdb1.geometry.isnull(), :]
    i_gdb2 = i_gdb2.copy()
    i_gdb2 = i_gdb2.loc[~i_gdb2.geometry.isnull(), :]
    o_gdb = i_gdb1
    if not all(isinstance(x, shpy.geometry.Point) for x in i_gdb1.geometry):
        if not all(isinstance(x, shpy.geometry.Point) for x in i_gdb2.geometry):
            raise NotImplementedError('The only supported geometries are shapely.geometry.Points')
    if verbose: print('Done!')

    if verbose: print('Converting to metric crs...', end='')
    i_gdb1 = i_gdb1.to_crs(epsg=3111)
    i_gdb2 = i_gdb2.to_crs(epsg=3111)
    if verbose: print('Done!')
    if verbose: print('Extracting coordinates...', end='')
    res1 = PP.parapandas(extract_point_coords, args=[i_gdb1])
    res2 = PP.parapandas(extract_point_coords, args=[i_gdb2])
    c_1 = res1[0]
    c_id_1 = res1[1]
    c_2 = res2[0]
    c_id_2 = res2[1]
    del res1, res2
    if verbose: print('Done!')

    ##  Find the matching points using the nearest neighbor algorithm (ball tree)
    if verbose: print('Run the nearest neighbor search...', end='')
    nbgrs = NearestNeighbors(n_neighbors=n_neighbors, radius=300, algorithm='auto').fit(c_2)
    if output_distance:
        distance, match = nbgrs.kneighbors(c_1, return_distance=True)
    else:
        match = nbgrs.kneighbors(c_1, return_distance=False)
    if verbose: print('Done!')

    if verbose: print('Preparing the output data...', end='')

    if n_neighbors == 1:
        o_gdb['IDX_MATCH'] = match
        try:
            mask = np.isnan(c_id_1)
        except TypeError:
            mask = c_id_1 == str(np.nan)
        o_gdb.loc[mask, 'IDX_MATCH'] = np.nan
    else:
        columns = ['IDX_MATCH_%s' %i for i in range(n_neighbors)]
        match_df = pd.DataFrame(match, columns=columns)
        for i in range(n_neighbors):
            o_gdb['IDX_MATCH_%s' %i] = match_df['IDX_MATCH_%s' %i]
            mask = np.isnan(c_id_1)
            o_gdb.loc[mask, 'IDX_MATCH_%s' %i] = np.nan
    if output_distance: 
        return o_gdb, distance
    return o_gdb
