# README #

### What is this repository for? ###

This is an ENEA specific basic functions repository

It contains:
1) BasicFuctions file, dedicated to general operations
2) BasicGeoFunctions file, for spatial operations
3) ParaPandas, a generalized method to parallelized operations on pandas and geopandas objects

### How do I get set up? ###

Simply go to your project, initialize the git repository for it, then run the command git submodule add git@bitbucket.org:enea-c/enea_analytics.git 
In this way you will have access to the functionalities offered by this repository, but will still be managed independently by git

Dependencies:
- Pandas
- GeoPandas
- Numpy
- Datetime
- Sklearn
- multiprocessing
- shapely

### Who do I talk to? ###

Current admin: Alessio Arena