'''Module dedicated to the interaction with S3 buckets
Please keep in mind that pandas supports already direct access to s3 filenames.
Just specify a valid s3 address i.e. s3://enea-au/test/asd.csv

This module is more for accessing entire folders
'''
import warnings
import io
import os
import boto3
from pandas.errors import EmptyDataError
import pandas as pd
import geopandas as gpd
from multiprocessing import Pool
from aar_analytics.utils import available_cpu_count

def _read_obj(obj):
    data = obj.get()['Body'].read()
    return data

def _read_csv(dic_arg):
    try:
        arg = dic_arg['data']
        kwargs = dic_arg['kwargs']
        out = pd.read_csv(arg, **kwargs)
        return out
    except EmptyDataError:
        warnings.warn('At least one of the filenames is empty')

def _read_excel(dic_arg):
    try:
        arg = dic_arg['data']
        kwargs = dic_arg['kwargs']
        out = pd.read_excel(arg, **kwargs)
        return out
    except EmptyDataError:
        warnings.warn('At least one of the filenames is empty')

def read_geopandas(bucket, filename, **kwargs):
    """This function reads zip files on s3 and decompress them in a local cache.
    Following that the gpd.read_file function will be called on the unzipped folder, returning the geodataframe
    
    Arguments:
    -----------
    bucket : str
        s3 bucket to access
    filename : str 
        path for the file
    **kwargs : int 
        arguments to be passed to gpd.read_file
    
    Returns:
    -----------
    out : gpd.GeoDataFrame
        output geodataframe
    """
    if not isinstance(filename, str):
        raise TypeError('The argument filename must be a string')
    if not filename.endswith('.zip'):
        raise TypeError('The file must be in zip format')
    path = read_zip(bucket, filename)
    print('Loading the file '+ path +' as GeoDataFrame...', end='')
    geodata = gpd.read_file(path, **kwargs)
    print('Done!')
    return geodata

def read_folder_as_pandas(bucket, folder, file_format='csv', verbose=True, force_single_proc=False, **kwargs):
    '''Read the files in a s3 folder into a pd.DataFrame
    Input:
    bucket      str - the bucket name to access
    folder      str - the path of filenames to retrieve. Works with startswith function, so has limited regex capabilities
    filename_format str, optional - csv, xls or xlsx

    Output:
    db          pd.DataFrame
    '''
    if not isinstance(file_format, str) or not file_format.lower() in ['csv', 'xls', 'xlsx']:
        raise ValueError('The argument filename_format is not recognized')
    print('Retrieving the remote filenames...')
    data = read_folder(bucket, folder, verbose)
    print('Converting them to DataFrames...')
    if force_single_proc:
        out = []
        for Istream in data:
            try:
                if file_format.lower() == 'csv':
                    out.append(pd.read_csv(Istream, **kwargs))
                elif file_format.lower() == 'xls' or file_format.lower() == 'xlsx':
                    out.append(pd.read_excel(Istream, **kwargs))
            except EmptyDataError:
                warnings.warn('One of the file is empty')
    else:
        p = Pool(available_cpu_count())
        p_data = list()
        for i in data:
            p_data.append(dict({'data': i, 'kwargs': kwargs}))
        if file_format.lower() == 'csv':
            out = p.map(_read_csv, p_data)
        elif file_format.lower() == 'xls' or file_format.lower() == 'xlsx':
            out = p.map(_read_excel, p_data)

    db = pd.concat(out)
    if verbose: print('Done!')
    return db


def read_folder(bucket, folder, verbose=True):
    '''Read the filenames in a s3 folder into a list of IOBytes objects.
    Those can be then passed into functions like pd.read_csv 
    Input:
    bucket      str - the bucket name to access
    folder      str - the path of filenames to retrieve. Works with startswith function, so has limited regex capabilities

    Output:
    out         list of IOBytes
    '''
    if not isinstance(folder, str):
        raise TypeError('The argument folder must be a string')
    if not isinstance(bucket, str):
        raise TypeError('The argument bucket must be a string')
    if not isinstance(verbose, bool):
        raise TypeError('The argument verbose must be a boolean')

    files = list_files(bucket, folder, return_data=True)
    out = []
    for fl in files:
        data = read_file(bucket, fl, verbose=verbose)
        out.append(data)
    if out:
        return out
    else:
        raise IOError('Could not find any filename matching the path specified')

def read_zip(bucket, filename):
    """This function will read a zip file from s3, and unzip it on a local folder (cache)
    If a file/folder of the same name exists, the unzipping will be skipped

    Arguments:
    -----------
    bucket : str
        bucket to acces on s3
    filename : str 
        path of the file to access

    Returns:
    -----------
    out : None
        description
    """
    if not isinstance(filename, str):
        raise TypeError('The argument filename must be a string')
    from zipfile import ZipFile
    out = ZipFile(read_file(bucket, filename))
    for name in out.namelist():
        if name.endswith('/'):
            fld_name = name
            break
    try:
        fld_name
        path = 'cache' + '/' + fld_name
        extract_to = 'cache'
    except NameError:
        fld_name = out.namelist()[0].split('.')[0]
        warnings.warn('The Zip filename content is not collected into a folder. Creating one called '+ fld_name)
        path = 'cache'+'/'+ fld_name
        extract_to = path
    if os.path.isdir(path):
        warnings.warn('The folder already exists, and the content of the zipfile will not be extracted. If you want to update it please remove the folder manually')
    else:
        out.extractall(extract_to)
    return path

def read_file(bucket, filename, verbose=False):
    '''Read one filename in s3 as a IOBytes object. This can be passed to functions like pd.read_csv
    Input:
    bucket      str - the bucket name to access
    folder      str - filename to retrieve. Works with startswith function, so has limited regex capabilities

    Output:
    out         IOBytes
    '''
    if not isinstance(filename, str):
        raise TypeError('The argument filename must be a string')
    s3 = bucket_exist(bucket)
    s3_bucket = s3.Bucket(bucket)
    s3_obj = s3_bucket.Object(filename)
    if verbose:
        print('Reading the filename '+filename+'...', end='')
    out = io.BytesIO(_read_obj(s3_obj))
    if verbose: 
        print('Done!')
    return out


def list_buckets():
    '''List of all the accessible buckets'''
    s3 = boto3.resource('s3')
    for bucket in s3.buckets.all():
        print(bucket.name)
    return None


def list_files(bucket, folder=False, return_data=False):
    '''List of all the objects contained in the specified bucket. You can specified a subfolder through the folder argument
    Input:
    bucket      str - s3 bucket name
    folder      str, optional - path. This works using startswith, so it has limited regex functionalities

    
    '''
    if not (isinstance(folder, bool) and not folder ) and not isinstance(folder, str):
        raise TypeError('the argument folder must be a string or False')
    s3 = bucket_exist(bucket)
    s3 = boto3.client('s3') # This is for the new fast access method implemented on 19/01
    if not folder:
        folder = ''
    try:
        key_list = [x['Key'] for x in s3.list_objects_v2(Bucket=bucket, Prefix=folder)['Contents']]
    except KeyError:
        raise IOError('Could not access the specified path')
    try:
        assert len(key_list) < 1000 # the fast access method is internally capped to 1000 objects. If we hit this limit we need to rerun using a paginator
    except AssertionError:
        paginator = s3.get_paginator('list_objects_v2')
        pageresponse = paginator.paginate(Bucket=bucket, Prefix=folder)
        pageresponse = iter(pageresponse)
        next(pageresponse)
        for pageobject in pageresponse:
            key_list.extend([x['Key'] for x in pageobject['Contents']])
    # cleaning up for folders
    key_list = [x for x in key_list if not x.endswith('/')]
    if return_data:
        return key_list
    for key in key_list:
        print(key)
    return None


def bucket_exist(bucket):
    '''Can you have access to the specified s3 bucket?
    Input:
    bucket      str - s3 bucket
    '''
    if not isinstance(bucket, str):
        raise TypeError('The bucket argument must be a string')
    try:
        import os
        aws_id = os.environ['AWS_ACCESS_KEY_ID']
        aws_key = os.environ['AWS_SECRET_ACCESS_KEY']
        s3 = boto3.resource('s3', aws_access_key_id=aws_id, aws_secret_access_key=aws_key)
    except:
        s3 = boto3.resource('s3')

    try:
        s3.meta.client.head_bucket(Bucket=bucket)
        return s3
    except:
        raise IOError('Could not read the bucket '+bucket)


def write_csv(df, bucket, folder, name):
    '''write one dataframe into a csv filename in s3 as a IOBytes object.
    Input:
    df          pd.DataFrame - input dataframe
    bucket      str - the bucket name to access
    folder      str - the path of the folder to write the filename in
    name        str - the name of the filename (myfile.csv)
    '''
    if not isinstance(df, pd.DataFrame) and not isinstance(df, pd.Series):
        raise TypeError('The input filename must be a pd.DataFrame or pd.Series')
    if not isinstance(bucket, str):
        raise TypeError('The argument bucket must be a string')
    if not isinstance(folder, str):
        raise TypeError('The argument folder must be a string')
    if not isinstance(name, str):
        raise TypeError('The argument name must be a string')
    
    if not folder.endswith('/'):
        folder += '/'
    csv_buffer = io.StringIO()
    df.to_csv(csv_buffer)
    s3_resource = bucket_exist(bucket)
    s3_resource.Object(bucket, folder + name).put(Body=csv_buffer.getvalue())
    return None


def write_parquet(df, bucket, folder, name):
    '''write one dataframe as a parquet filename into s3 as a IOBytes object.
    Input:
    df          pd.DataFrame - input dataframe
    bucket      str - the bucket name to access
    folder      str - the path of the folder to write the filename in
    name        str - the name of the filename (myfile.csv)
    '''
    if not isinstance(df, pd.DataFrame) and not isinstance(df, pd.Series):
        raise TypeError('The input filename must be a pd.DataFrame or pd.Series')
    if not isinstance(bucket, str):
        raise TypeError('The argument bucket must be a string')
    if not isinstance(folder, str):
        raise TypeError('The argument folder must be a string')
    if not isinstance(name, str):
        raise TypeError('The argument name must be a string')
    
    try:
        import pyarrow
        import pyarrow.parquet
    except ImportError:
        raise ImportError('pyarrow is missing from your system')
    s3_resource = bucket_exist(bucket)

    if not folder.endswith('/'):
        folder += '/'
    try:
        pyarrow_table = pyarrow.Table.from_pandas(df)
    except pyarrow.ArrowInvalid:
        warnings.warn('Conversion to parquet failed. Saving as csv instead')
        name = name.replace('.parquet', '.csv')
        write_csv(df, bucket, folder, name)
        return None
    buffer = pyarrow.BufferOutputStream()
    pyarrow.parquet.write_table(pyarrow_table, buffer)

    remote_buffer = bytes(buffer.get_result())

    s3_resource.Object(bucket, folder + name).put(Body=remote_buffer)
    return None
