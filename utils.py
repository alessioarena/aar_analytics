import re
import os
import warnings
import pandas as pd
from pyarrow import ArrowIOError
from botocore.client import ClientError

try:
    os.environ['S3_BUCKET']
except KeyError:
    os.environ['S3_BUCKET'] = 'pcor-dev-proj-fcast'

def available_cpu_count():
    """Utility to find out how many accessible CPUs there are.
    It will try cpuset(LINUX) first, in case the accessible ones are limited
    Secondly, it will use the multiprocessing.cpu_counts().
    Finally it will try to retrieve it from the OS environment (POSIX, Windows, Linux, Unix)

    Returns:
    -----------
    out : int
        number of available CPUs
    """

    # cpuset
    # cpuset may restrict the number of *available* processors
    try:
        m = re.search(r'(?m)^Cpus_allowed:\s*(.*)$',
                      open('/proc/self/status').read())
        if m:
            res = bin(int(m.group(1).replace(',', ''), 16)).count('1')
            if res > 0:
                return res
    except IOError:
        pass

    # Python 2.6+
    try:
        import multiprocessing
        return multiprocessing.cpu_count()
    except (ImportError, NotImplementedError):
        pass

    # POSIX
    try:
        res = int(os.sysconf('SC_NPROCESSORS_ONLN'))

        if res > 0:
            return res
    except (AttributeError, ValueError):
        pass

    # Windows
    try:
        res = int(os.environ['NUMBER_OF_PROCESSORS'])

        if res > 0:
            return res
    except (KeyError, ValueError):
        pass

    # Linux
    try:
        res = open('/proc/cpuinfo').read().count('processor\t:')

        if res > 0:
            return res
    except IOError:
        pass

    # Other UNIXes (heuristic)
    try:
        try:
            dmesg = open('/var/run/dmesg.boot').read()
        except IOError:
            import subprocess
            dmesgProcess = subprocess.Popen(['dmesg'], stdout=subprocess.PIPE)
            dmesg = dmesgProcess.communicate()[0]

        res = 0
        while '\ncpu' + str(res) + ':' in dmesg:
            res += 1

        if res > 0:
            return res
    except OSError:
        pass

    raise Exception('Can not determine number of CPUs on this system')


def _open_data_helper(bucket, path):

    if path.endswith(('.xls', 'xlsx')):
        return pd.read_excel
    elif path.endswith(('.csv','.txt')):
        return pd.read_csv
    elif path.endswith('.parquet'):
        return pd.read_parquet
    elif path.endswith('.zip'):
        from aar_analytics.AWSFunctions import read_geopandas
        return read_geopandas
    elif path.endswith('/') or re.search('\.', path) is None:
        try:
            from aar_analytics.AWSFunctions import list_files
            i_file = list_files(bucket, path, return_data=True)[0]
        except (FileNotFoundError, ArrowIOError):
            raise NotImplementedError('At the moment I cannot work with local folders. Still working on this')
            #i_file = os.listdir(path)
        func = _open_data_helper(bucket, i_file)
        if func == pd.read_csv:
            from aar_analytics.AWSFunctions import read_folder_as_pandas
            return (read_folder_as_pandas, 'csv')
        if func == pd.read_excel:
            from aar_analytics.AWSFunctions import read_folder_as_pandas
            return (read_folder_as_pandas, 'xls')

        else:
            raise NotImplementedError('For the moment I can load only folders containing csv files')
    else:
        raise NotImplementedError('Unknown format')


def open_data(path, **kwargs):
    """Function to open data automatically based on extension.
    It will try on s3, then local cache, and finally throwing a FileNotFoundError
    if parquet, it will try to load it but will fall back to csv if not found

    Arguments:
    -----------
    path : str
        file path on s3 (do not include the bucket)
    **kwargs : kwargs, optional
        keyword arguments to be passed to the input parser function. This can be read_csv, read_excel or read_parquet from pandas

    Returns:
    -----------
    out : pd.DataFrame
        opened object
    """
    if not isinstance(path, str):
        raise TypeError('The path argument must be a string')

    bucket = 'pcor-dev-proj-fcast'
    try:
        import os
        bucket = os.environ['S3_BUCKET']
    except KeyError:
        pass
    try:        
        import Settings
        bucket = Settings.bucket
    except ImportError:
        pass

    func = _open_data_helper(bucket, path)

    try:
        if isinstance(func, tuple):
            data = func[0](bucket, path, file_format=func[1], **kwargs)
            return data
        elif func.__name__ == 'read_geopandas':
            data = func(bucket, path)
            return data
        elif func.__name__ == 'read_parquet':
            try:
                s3_path = 's3://' + make_path(bucket, path)
                data = func(s3_path, **kwargs)
                return data
            except (ArrowIOError, ClientError, FileNotFoundError):
                pass
            s3_path = s3_path.replace('.parquet', '.csv')
            data = pd.read_csv(s3_path)
            data = data.iloc[:, 1:]
            return data
        else:
            s3_path = 's3://' + make_path(bucket, path)
            data = func(s3_path, **kwargs)
            return data
    except FileNotFoundError:
        warnings.warn('Could not find the file '+path+' on s3. Trying locally...')
        pass

    filename = path.split('/')[-1]
    local_path = 'data/'+filename
    try:
        data = func(local_path, **kwargs)
        return data
    except (FileNotFoundError, ArrowIOError):
        raise FileNotFoundError('Could not find the file'+local_path)


def make_path(*args):
    """Utility to create a path string from multiple strings

    Arguments:
    -----------
    *args : str
        x number of strings to form the path

    Returns:
    -----------
    out : str
        string for the path
    """
    if isinstance(args[0], list):
        args = args[0]
    # if len(args) < 2:
    #     raise RuntimeError('A minimum of two strings is needed')
    path = args[0]
    for arg in args[1:]:
        if not isinstance(arg, str):
            raise TypeError('Only strings are valid inputs')
        if arg.startswith('/'):
            arg = arg[1:]
        if arg.endswith('/'):
            arg = arg[:-1]
        path += '/' + arg
    return path


def save_parquet(df, path):
    if not isinstance(df, pd.DataFrame) and not isinstance(df, pd.Series):
        raise TypeError('The input file must be a pd.DataFrame or pd.Series')
    if not isinstance(path, str):
        raise TypeError('The argument folder must be a string')

    try:
        import pyarrow
        import pyarrow.parquet
    except ImportError:
        raise ImportError('pyarrow is missing from your system')

    try:
        pyarrow_table = pyarrow.Table.from_pandas(df)
    except pyarrow.ArrowInvalid:
        warnings.warn('Conversion to parquet failed. Saving as csv instead')
        path = path.replace('.parquet', '.csv')
        df.to_csv(path)
        return None
    pyarrow.parquet.write_table(pyarrow_table, path)
    return None


def coerce_to_series(data):
    #dataframe of one column
    try:
        if len(data.columns) == 1:
            data = data.squeeze()
        else:
            raise RuntimeError
        return data
    except: # pylint : disable=W0702
        pass

    #1D array, or 2D array with one D of lenght 1
    try:
        if len(data.shape) == 1:
            data = pd.Series(data)
        elif data.shape(0) == 1 or data.shape(1) == 1:
            data = pd.Series(data.reshape(-1))
        else:
            raise RuntimeError
        return data
    except: # pylint : disable=W0702
        pass

    raise RuntimeError('Could not coerce the input data to pd.Series')

