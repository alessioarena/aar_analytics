
import warnings
from multiprocessing import Pool
import pandas as pd
from geopandas import GeoDataFrame
import numpy as np
from aar_analytics.utils import available_cpu_count



def _parapandas_helper(dict_of_args):
    func = dict_of_args['func']
    args = dict_of_args['args']

    try:
        kwargs = dict_of_args['kwargs']
    except KeyError:
        kwargs = False
    if kwargs:
        return func(*args, **kwargs)
    return func(*args)

def _parapandas_partitioner(args, split, NUM_PARTITIONS):

    if not args or not isinstance(args, list):
        raise TypeError('args must be a list')

    partitioned_args = []

    if isinstance(split, bool):
        if split:
            for db in args:
                if isinstance(db, GeoDataFrame):
                    chunk_size = int(np.floor(len(db)/NUM_PARTITIONS))
                    geodb_part = []
                    for i in range(NUM_PARTITIONS-1):
                        chunk = db.iloc[chunk_size*i:chunk_size*(i+1)]
                        geodb_part.append(chunk)
                    geodb_part.append(db.iloc[chunk_size*(i+1):])
                    partitioned_args.append(geodb_part)
                else:
                    partitioned_args.append(np.array_split(db, NUM_PARTITIONS))
        else:
            raise ValueError('split cannot be a single False value')


    elif isinstance(split, list) and all(isinstance(x, bool) for x in split):
        if np.sum(args) > 0:
            if len(split) == 1:
                for db in args:
                    if isinstance(db, GeoDataFrame):
                        chunk_size = int(np.floor(len(db)/NUM_PARTITIONS))
                        geodb_part = []
                        for i in range(NUM_PARTITIONS-1):
                            chunk = db.iloc[chunk_size*i:chunk_size*(i+1)]
                            geodb_part.append(chunk)
                        geodb_part.append(db.iloc[chunk_size*(i+1):])
                        partitioned_args.append(geodb_part)
                    else:
                        partitioned_args.append(np.array_split(db, NUM_PARTITIONS))
            elif len(split) == len(args):
                if len(args) == 1:
                    raise ValueError('You passed one argument but multiple values in the split array')
                elif len(args) > 1:
                    for db, bol in zip(args, split):
                        if bol:
                            if isinstance(db, GeoDataFrame):
                                chunk_size = int(np.floor(len(db)/NUM_PARTITIONS))
                                geodb_part = []
                                for i in range(NUM_PARTITIONS-1):
                                    chunk = db.iloc[chunk_size*i:chunk_size*(i+1)]
                                    geodb_part.append(chunk)
                                geodb_part.append(db.iloc[chunk_size*(i+1):])
                                partitioned_args.append(geodb_part)
                            else:
                                partitioned_args.append(np.array_split(db, NUM_PARTITIONS))
                        else:
                            partitioned_args.append(db)
            else:
                raise ValueError('split array and args array have different length')
        else:
            raise ValueError('split array must have at least one True value')
    else:
        raise TypeError('split must be a bool or list of bool')
    return partitioned_args

def _parapandas_appender(results):
    if not isinstance(results, list):
        raise TypeError('You should not see this message. Results from workers are not in the form of an array')

    if all(isinstance(x, pd.DataFrame) for x in results):
        o_db = pd.concat(results)
    elif all(isinstance(x, np.ndarray) for x in results):
        o_db = np.concatenate(results)
    elif all(isinstance(x, tuple) for x in results):
        o_db = []
        for res, _ in enumerate(results[0]):
            for tpl in results:
                try:
                    if isinstance(tpl[res], pd.DataFrame):
                        o_db_col = pd.concat([o_db_col, tpl[res]]) #pylint: disable=E0601
                    elif isinstance(tpl[res], np.ndarray):
                        o_db_col = np.concatenate([o_db_col, tpl[res]])
                    else:
                        warnings.warn('Could not concatenate the results. Returning a non concatenated result', RuntimeWarning, stacklevel=2)
                        return results
                except NameError:
                    o_db_col = tpl[res]
            o_db.append(o_db_col)
            del o_db_col
        return o_db
    else:
        warnings.warn('Could not concatenate the results. Returning a non concatenated result', RuntimeWarning, stacklevel=2)
        return results
    return o_db

def _parapandas_reshaper(func, partitioned_args, kwargs, NUM_PARTITIONS):
    if not callable(func):
        raise TypeError('func must be a callable object')
    if not isinstance(partitioned_args, list):
        raise TypeError('partitioned_args is not a list')
    if kwargs and not isinstance(kwargs, dict):
        raise TypeError('kwargs must be a dictionary')

    parapandas_args = []

    for j in range(NUM_PARTITIONS):
        reshaped_partitioned_args = []
        dict_of_args = {}
        for i, p_a in enumerate(partitioned_args):
            if len(p_a) == NUM_PARTITIONS:
                reshaped_partitioned_args.append(p_a[j])
            else:
                reshaped_partitioned_args.append(p_a)
        dict_of_args['func'] = func
        dict_of_args['args'] = reshaped_partitioned_args
        if kwargs: dict_of_args['kwargs'] = kwargs
        parapandas_args.append(dict_of_args)
    return parapandas_args

def parapandas(func, args, kwargs=False, split=True, o_print=False, NUM_WORKER_JOBS=5, NUM_CPUS=available_cpu_count()):
    '''Multithreaded for row_by_row operations on pandas DataFrame(s)\n
    Input\n
    func:           callable object - function to be performed on each row
    args:           list - input arguments for func
    kwargs:         dict - additional keyword arguments for func (optional, False)
    split:          bool or list of bool - map of arguments to be split among the workers (optional, True)
    o_print:        bool - verbose option (optional, False)
    NUM_PARTITIONS: int - number of partitions for the multithreaded run (optional, 20)
    NUM_CPUS:       int - number of worker to use for the multithreaded run (optional, 4)
    Output:\n
    o_db:           gpd.GeoDataFrame, np.ndarray or array of them(if multiple dataset are return by func)
    -------------
    Example usage
    -------------

    >>> type(extract_coords)
        method
    >>> type(input_gdb)
        GeoDataFrame
    >>> list_of_coords = parapandas(extract_coords, [input_gdb], {'o_print':False})

    '''
    if not callable(func):
        raise TypeError('func must be a callable object')
    if kwargs and not isinstance(kwargs, dict):
        raise TypeError('kwargs must be a dictionary')
    #if len(args) == 0 or (isinstance(args, pd.DataFrame) and args.empty):
    #    raise ValueError('args is an empty array or pd.DataFrame')
    if not isinstance(NUM_CPUS, int) or NUM_CPUS == 0:
        raise TypeError('NUM_CPUS must the an integer number different than zero')
    if not isinstance(NUM_WORKER_JOBS, int) or NUM_WORKER_JOBS == 0:
        raise TypeError('NUM_PARTITIONS must the an integer number different than zero')
    if not isinstance(o_print, bool):
        raise TypeError('o_print must be a bool')
    if not isinstance(split, bool) and (not isinstance(split, list) or all(isinstance(x, bool) for x in split)):
        raise TypeError('split must be a bool or list of bool')

    # if the file has less rows than the number of partitions
    for i in args:
        while NUM_CPUS > len(i):
            NUM_CPUS = round(NUM_CPUS / 2)
        while (NUM_WORKER_JOBS * NUM_CPUS) > len(i):
            NUM_WORKER_JOBS -= 1
    NUM_PARTITIONS = NUM_WORKER_JOBS * NUM_CPUS

    ## splitting the input args into number or partitions
    partitioned_args = _parapandas_partitioner(args, split, NUM_PARTITIONS)

    ## 1) reshaped the partitioned args
    ## 2) build the dictionary for each worker
    ####  func - callable object
    ####  args - reshaped array of partitioned arguments
    ####  kwargs - dictionary
    ## 3) build the array of dictionaries
    parapandas_args = _parapandas_reshaper(func, partitioned_args, kwargs, NUM_PARTITIONS)

    ## Open the pool of workers
    p = Pool(NUM_CPUS)

    ## map the jobs to the workers and execute them
    results = p.map(_parapandas_helper, parapandas_args)

    ## concatenate results where possible
    o_db = _parapandas_appender(results)

    return o_db
