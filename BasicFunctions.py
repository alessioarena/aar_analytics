"""Additional utilities for pandas
"""

import pandas as pd
import numpy as np


# This is an alternative to pd.Series.value_counts(). It adds a column sign between fields and field counts.
# You can then copy and paste the result to excel for easy conversion of text to data
def column_value_count(i_series, o_print=True):
    """Alternative to pandas.Series.value_counts(), to allow more control on the formatting
    Input:  pd.Series or list
    Output: Dictionary - Counter"""
    if isinstance(i_series, (list,pd.Series)):
        if not i_series.empty:
            count = {}
            for row in i_series:
                if not pd.isnull(row):
                    try:
                        count[row] += 1
                    except KeyError:
                        count.update({str(row) : 0})
                        count[row] += 1
        else:
            raise ValueError('The input argument is empty')
    else:
        raise TypeError('The input argument must be a pd.Series or list type')
    if o_print:
        for el in count:
            print(str(el)+':'+str(count[el]))
    return count

# This performs a count of 1D array vs 1D array, resulting in a 2D array of value_counts()
def two_columns_value_count(i_series, i2_series, o_print=True):
    """Utility to count the value pairs from two series

    Arguments:
    -----------
    i_series : pd.Series
        first series
    i2_series : pd.Series 
        second series
    o_print : bool, optional (default : Value)\n
        print the output instead of returning it as a dataframe

    Returns:
    -----------
    out : pd.DataFrame
        if o_print is False
    """
    if not isinstance(i_series, pd.Series) or not isinstance(i2_series, pd.Series):
        raise TypeError('The two input must be a pd.Series')
    if i_series.empty or i2_series.empty:
        raise ValueError('At least one of the two input is empty')
    if not len(i_series) == len(i2_series):
        raise ValueError('The two input must have the same length')

    o_Counter = pd.DataFrame(data=np.zeros((len(i_series.unique()), len(i2_series.unique())), dtype=int), index=i_series.unique(), columns=i2_series.unique())
    for x, y in zip(i_series.values, i2_series.values):
        o_Counter.loc[x, y] += 1

    if o_print:
        print(o_Counter)
        return None
    return o_Counter


def dataframe_dtypes(df, o_print=True):
    """Utility to print the summary of types of each element in a dataframe.
    This can be different from the dtype attribute in pandas, as the column type and the element type are two different things 

    Arguments:
    -----------
    df : pd.DataFrame or pd.Series
        input dataframe
    o_print : bool, optional (default : True)\n
        print the output instead of returning it as a dataframe

    Returns:
    -----------
    out : pd.DataFrame
        if o_print is False
    """
    if not isinstance(df, pd.DataFrame) and not isinstance(df, pd.Series):
        raise TypeError('The input data is not a DataFrame or Series')
    if df.empty:
        raise ValueError('The dataframe is empty')
    if not isinstance(o_print, bool):
        raise TypeError('The argument o_print must be a boolean')

    if isinstance(df, pd.DataFrame):
        df_dtypes = pd.DataFrame()
        for col in df.columns:
            col_dtypes = df[col].apply(type).value_counts()
            df_dtypes = pd.concat([df_dtypes, col_dtypes], axis=1)
    else:
        df_dtypes = df.apply(type).value_counts()

    if o_print:
        print(df_dtypes)
        return None
    return df_dtypes
